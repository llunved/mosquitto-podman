ARG OS_RELEASE=34
ARG OS_IMAGE=fedora:$OS_RELEASE

FROM $OS_IMAGE as build

ARG OS_RELEASE
ARG OS_IMAGE
ARG HTTP_PROXY=""

LABEL MAINTAINER riek@llunved.net

ENV LANG=C.UTF-8
ENV VOLUMES="/etc/mosquitto,/var/log/mosquitto"

USER root

RUN mkdir -p /mosquitto
WORKDIR /mosquitto

ADD ./rpmreqs-build.txt /mosquitto/

ENV http_proxy=$HTTP_PROXY
RUN dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$OS_RELEASE.noarch.rpm \
    https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$OS_RELEASE.noarch.rpm \
    && dnf -y upgrade \
    && dnf -y install $(cat rpmreqs-build.txt) 

ADD ./rpmreqs-rt.txt ./rpmreqs-dev.txt /mosquitto/
# Create the minimal target environment
RUN mkdir /sysimg \
    && dnf install --installroot /sysimg --releasever $OS_RELEASE --setopt install_weak_deps=false --nodocs -y coreutils-single glibc-minimal-langpack $(cat rpmreqs-rt.txt) \
    && if [ "${DEVBUILD}" == "True" ]; then dnf install --installroot /sysimg --releasever $OS_RELEASE --setopt install_weak_deps=false --nodocs -y $(cat rpmreqs-dev.txt); fi \
    && rm -rf /sysimg/var/cache/*

#FIXME this needs to be more elegant
RUN ln -s /sysimg/usr/share/zoneinfo/America/New_York /sysimg/etc/localtime

# Move the mosquitto config to a deoc dir, so we can mount config from the host but export the defaults from the host
RUN if [ -d /sysimg/usr/share/doc/mosquitto ]; then \
       mv /sysimg/usr/share/doc/mosquitto /sysimg/usr/share/doc/mosquitto.default ; \
    else \
       mkdir -p /sysimg/usr/share/doc/mosquitto.default ; \
    fi ; \
    mkdir /sysimg/usr/share/doc/mosquitto.default/config

RUN for CURF in ${VOLUMES} ; do \
    if [ "$(ls -A /sysimg${CURF})" ]; then \
        mkdir -pv /sysimg/usr/share/doc/mosquitto.default/config${CURF} ; \
        mv -fv /sysimg${CURF}/* /sysimg/usr/share/doc/mosquitto.default/config${CURF}/ ;\
    fi ; \
    done

ADD ./chown_dirs.sh \
    ./init_container.sh /sysimg/sbin
 
RUN chmod +x /sysimg/sbin/chown_dirs.sh \
    /sysimg/sbin/init_container.sh 
 
# Set up systemd inside the container
ADD init_container.service \
    chown_dirs.service \
    /sysimg/etc/systemd/system

RUN /usr/bin/systemctl --root /sysimg mask systemd-remount-fs.service dev-hugepages.mount sys-fs-fuse-connections.mount systemd-logind.service getty.target console-getty.service && systemctl --root /sysimg disable dnf-makecache.timer dnf-makecache.service
RUN /usr/bin/systemctl --root /sysimg enable mosquitto.service chown_dirs.service init_container.service
 

FROM scratch AS runtime

COPY --from=build /sysimg /

WORKDIR /root

ENV CHOWN=true 
ENV CHOWN_DIRS="/var/log/mosquitto,/etc/mosquitto,/etc/pki/tls/certs/mosquitto.pem,/etc/pki/tls/private/mosquitto.key" 
ENV CHOWN_USER="mosquitto"
 
ENV VOLUMES="/etc/mosquitto,/var/log/mosquitto,"

VOLUME /etc/mosquitto
VOLUME /var/log/mosquitto

 
EXPOSE 1883 8883
CMD ["/usr/sbin/init"]
STOPSIGNAL SIGRTMIN+3


